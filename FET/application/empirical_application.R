

library(rugarch)
library(GAS)
library(esregression)
library(quantreg)
library(esencmp)

source("recursive_models.R")

# 2. set  options
n_IS    <- 2000
alpha   <- 0.025


# write a loop that runs over all assets, the stock returns (IBM and APPL), the indices
# (SP500 and DJIA) and the exchange rates (EURUSD and EURGBP)


assets.FX        <- c("EURUSD", "EURGBP")
assets.indices   <- c("SP500","DJIA")
assets.stocks    <- c("IBM","APPL","WMT","MMM","MSFT")
assets.foreign   <- c("DAX", "CAC", "NIKKEI")
assets           <- c("SP500","IBM","DAX")
n.assets         <- length(assets)


# write tables:
# rows:
dgp_names <-c("HS", "RM", "GJR", "GAS", "G1F", "G2F", "ASES", "SAVES")
emtpy_col <- c(NA, NA, NA, NA, NA, NA, NA, NA )



# 1. read in return or price time series for the individual stocks (IBM and APPL), the SP500, the DJIA
#    and the EURUSD and EURGBP exchange rate

#    period: 06/01/2000 until 05/31/2019

# IBM
prices_IBM   <- read.csv("data/IBM.csv")
prices_IBM   <- prices_IBM[-c(1:106,4887:4949),]
ret_IBM      <- diff(log(prices_IBM$Close))

# APPL
prices_APPL  <- read.csv("data/APPL.csv")
prices_APPL  <- prices_APPL[-c(1:106,4887:4949),]
ret_APPL     <- diff(log(prices_APPL$Close))

# WMT
prices_WMT   <- read.csv("data/WMT.csv")
ret_WMT      <- diff(log(prices_WMT$Close))

# MMM
prices_MMM   <- read.csv("data/MMM.csv")
ret_MMM      <- diff(log(prices_MMM$Close))

# MSFT
prices_MSFT  <- read.csv("data/MSFT.csv")
ret_MSFT     <- diff(log(prices_MSFT$Close))

# S&P500:
prices_SP500 <- read.csv("data/SP500.csv")
prices_SP500 <- prices_SP500[-c(1:106,4887:4949),]
ret_SP500    <- diff(log(prices_SP500$Close))

# DJIA:
prices_DJIA  <- read.csv("data/DJIA.csv")
prices_DJIA  <- prices_DJIA[-c(1:106,4887:4949),]
ret_DJIA     <- diff(log(prices_DJIA$Close))

# DAX:
DAX.raw <- read.csv("data/GDAXI.csv")
DAX <- DAX.raw[c("Date","Close")]
DAX <- DAX[(DAX$Close != "null"),]
DAX <- DAX[-c(1:110,4974:5036),]

prices_DAX <- as.numeric(paste(DAX$Close))
ret_DAX    <- diff(log(as.numeric(prices_DAX)))

# CAC:
CAC.raw <- read.csv("data/FCHI.csv")
CAC     <- CAC.raw[c("Date","Close")]
CAC     <- CAC[(CAC$Close != "null"),]
CAC     <- CAC[-c(1:110,4996:5036),]

prices_CAC <- as.numeric(paste(CAC$Close))
ret_CAC    <- diff(log(as.numeric(prices_CAC)))



# FTSE:
#FTSE.raw <- read.csv("data/FTSE.csv")
#FTSE <- FTSE.raw[c("Date","Close")]
#FTSE <- FTSE[(FTSE$Close != "null"),]
#FTSE <- FTSE[-c(1:110,4974:5036),]

#prices_FTSE <- as.numeric(paste(FTSE$Close))
#ret_FTSE    <- diff(log(as.numeric(prices_FTSE)))


# NIKKEI:
NIKKEI.raw <- read.csv("data/N225.csv")
NIKKEI <- NIKKEI.raw[c("Date","Close")]
NIKKEI <- NIKKEI[(NIKKEI$Close != "null"),]
NIKKEI <- NIKKEI[-c(1:110,4887:4949),]

prices_NIKKEI <- as.numeric(paste(NIKKEI$Close))
ret_NIKKEI    <- diff(log(as.numeric(prices_NIKKEI)))


# EURUSD:
prices_EURUSD <- read.csv("data/EUR_USD Historical Data.csv")
prices_EURUSD <- prices_EURUSD[-c(1:30,4989:5000),]
prices_EURUSD <- prices_EURUSD[dim(prices_EURUSD)[1]:1,]
ret_EURUSD    <- diff(log(prices_EURUSD$Price))

# EURGBP:
prices_EURGBP <- read.csv("data/EUR_GBP Historical Data.csv")
prices_EURGBP <- prices_EURGBP[-c(1:30,4989:5000),]
prices_EURGBP <- prices_EURGBP[dim(prices_EURGBP)[1]:1,]
ret_EURGBP    <- diff(log(prices_EURGBP$Price))



# initialize forecasts lists
FC <- list()

for (i.asset in 1:n.assets){

asset_choice <- assets[i.asset]


# specify a time series to be run:

if (asset_choice == "IBM"){

  ret <- 100*ret_IBM

} else if (asset_choice == "APPL"){

  ret <- 100*ret_APPL

} else if (asset_choice == "MMM"){

  ret <- 100*ret_MMM

} else if (asset_choice == "WMT"){

  ret <- 100*ret_WMT

} else if (asset_choice == "MSFT"){

  ret <- 100*ret_MSFT

} else if (asset_choice == "SP500"){

  ret <- 100*ret_SP500

} else if (asset_choice == "DJIA"){

  ret <- 100*ret_DJIA

} else if (asset_choice == "EURUSD"){

  ret <- 100*ret_EURUSD

} else if (asset_choice == "EURGBP"){

  ret <- 100*ret_EURGBP

} else if (asset_choice == "DAX"){

  ret <- 100*ret_DAX

} else if (asset_choice == "CAC"){

  ret <- 100*ret_CAC

} else if (asset_choice == "FTSE"){

  ret <- 100*ret_FTSE

} else if (asset_choice == "NIKKEI"){

  ret <- 100*ret_NIKKEI

}


ret    <- as.vector(unlist(ret))
n_days <- length(ret)
n_OOS  <- n_days - n_IS



# 3. Run the Encompassing Tests--------------------------------------------------------------------------------

if (sum(asset_choice == assets.stocks) > 0){
  # Initialize DFs for the VaR and ES forecasts
  q_FC <- data.frame(HistSim=numeric(n_OOS),RiskMetrics=numeric(n_OOS),
                     GJRGARCH_t=numeric(n_OOS), #EGARCH_t=numeric(n_OOS),
                     GAS_t=numeric(n_OOS),
                     GAS1F=numeric(n_OOS), GAS2F=numeric(n_OOS),
                     CAREAS=numeric(n_OOS), CARESAV=numeric(n_OOS))
  e_FC <- data.frame(HistSim=numeric(n_OOS),RiskMetrics=numeric(n_OOS),
                     GJRGARCH_t=numeric(n_OOS), #EGARCH_t=numeric(n_OOS),
                     GAS_t=numeric(n_OOS),
                     GAS1F=numeric(n_OOS), GAS2F=numeric(n_OOS),
                     CAREAS=numeric(n_OOS), CARESAV=numeric(n_OOS))
} else if (sum(asset_choice == assets.indices) > 0){
  # Initialize DFs for the VaR and ES forecasts
  q_FC <- data.frame(HistSim=numeric(n_OOS),RiskMetrics=numeric(n_OOS),
                     GJRGARCH_t=numeric(n_OOS), #EGARCH_t=numeric(n_OOS),
                     GAS_t=numeric(n_OOS),
                     GAS1F=numeric(n_OOS), GAS2F=numeric(n_OOS),
                     CAREAS=numeric(n_OOS), CARESAV=numeric(n_OOS))
  e_FC <- data.frame(HistSim=numeric(n_OOS),RiskMetrics=numeric(n_OOS),
                     GJRGARCH_t=numeric(n_OOS), #EGARCH_t=numeric(n_OOS),
                     GAS_t=numeric(n_OOS),
                     GAS1F=numeric(n_OOS), GAS2F=numeric(n_OOS),
                     CAREAS=numeric(n_OOS), CARESAV=numeric(n_OOS))
}  else if (sum(asset_choice == assets.foreign) > 0){
  # Initialize DFs for the VaR and ES forecasts
  q_FC <- data.frame(HistSim=numeric(n_OOS),RiskMetrics=numeric(n_OOS),
                     GJRGARCH_t=numeric(n_OOS), #EGARCH_t=numeric(n_OOS),
                     GAS_t=numeric(n_OOS),
                     GAS1F=numeric(n_OOS), GAS2F=numeric(n_OOS),
                     CAREAS=numeric(n_OOS), CARESAV=numeric(n_OOS))
  e_FC <- data.frame(HistSim=numeric(n_OOS),RiskMetrics=numeric(n_OOS),
                     GJRGARCH_t=numeric(n_OOS), #EGARCH_t=numeric(n_OOS),
                     GAS_t=numeric(n_OOS),
                     GAS1F=numeric(n_OOS), GAS2F=numeric(n_OOS),
                     CAREAS=numeric(n_OOS), CARESAV=numeric(n_OOS))
} else if (sum(asset_choice == assets.FX) >0){
  # Initialize DFs for the VaR and ES forecasts
  q_FC <- data.frame(GJRGARCH_t=numeric(n_OOS), #EGARCH_t=numeric(n_OOS),
                     RiskMetrics=numeric(n_OOS), HistSim=numeric(n_OOS),
                     GAS1F=numeric(n_OOS), #GAS2F=numeric(n_OOS),
                     CAREAS=numeric(n_OOS), CARESAV=numeric(n_OOS),
                     GAS_t=numeric(n_OOS))
  e_FC <- data.frame(GJRGARCH_t=numeric(n_OOS), #EGARCH_t=numeric(n_OOS),
                     RiskMetrics=numeric(n_OOS), HistSim=numeric(n_OOS),
                     GAS1F=numeric(n_OOS), #GAS2F=numeric(n_OOS),
                     CAREAS=numeric(n_OOS), CARESAV=numeric(n_OOS),
                     GAS_t=numeric(n_OOS))
}


  # GJRGARCH(1,1)-t model with fixed estimation
  GJRGARCH_t.spec <- ugarchspec(mean.model = list(include.mean = FALSE,armaOrder=c(0,0)), variance.model = list(model = "gjrGARCH"), 
                                distribution.model = "std")
  GJRGARCH_t.fit  <- ugarchfit(spec=GJRGARCH_t.spec, data=ret, out.sample=n_OOS)
  GJRGARCH_t.FC   <- ugarchforecast(GJRGARCH_t.fit, data = NULL, n.ahead = 1, n.roll = n_OOS, out.sample = n_OOS)
  q_FC$GJRGARCH_t <- qnorm(alpha) * head(as.numeric(sigma(GJRGARCH_t.FC)[1,]),-1)
  e_FC$GJRGARCH_t <--dnorm(qnorm(alpha))/alpha *  head(as.numeric(sigma(GJRGARCH_t.FC)[1,]),-1)
  # we cut the last OOS forecast here (T*+1), so the OOS forecasts from all models have the same length

  # if (sum(asset_choice == assets.FX) == 0){
  # # EGARCH(1,1)-t model with fixed estimation
  # EGARCH_t.spec <- ugarchspec(mean.model = list(include.mean = FALSE,armaOrder=c(0,0)), variance.model = list(model = "eGARCH"), distribution.model = "std")
  # EGARCH_t.fit  <- ugarchfit(spec=EGARCH_t.spec, data=ret, out.sample=n_OOS)
  # EGARCH_t.FC   <- ugarchforecast(EGARCH_t.fit, data = NULL, n.ahead = 1, n.roll = n_OOS, out.sample = n_OOS)
  # q_FC$EGARCH_t <- qnorm(alpha) * head(as.numeric(sigma(EGARCH_t.FC)[1,]),-1)
  # e_FC$EGARCH_t <--dnorm(qnorm(alpha))/alpha *  head(as.numeric(sigma(EGARCH_t.FC)[1,]),-1)
  # }

  # Estimate and Forecast a RiskMetrics Model
  RiskMetrics.spec <- ugarchspec(mean.model = list(include.mean = FALSE,armaOrder=c(0,0)),
                                 fixed.pars = list(omega=0, alpha1=0.06, beta1=0.94) )
  RiskMetrics.FC   <- ugarchforecast(RiskMetrics.spec, data = ret, n.ahead = 1, n.roll=n_OOS, out.sample=n_OOS)
  q_FC$RiskMetrics <- qnorm(alpha) * head(as.numeric(sigma(RiskMetrics.FC)[1,]),-1)
  e_FC$RiskMetrics <- -dnorm(qnorm(alpha))/alpha * head(as.numeric(sigma(RiskMetrics.FC)[1,]),-1)

  # Forecast a Historical Simulation Model for the VaR and ES
  HistSim_QES <- function(x,alpha) {
    x_Q       <- quantile(x,alpha)
    x_ES      <- mean(x[x <= x_Q])
    return(list(Q=x_Q, ES=x_ES))
  }
  
  for (t in 1:(n_OOS) ) {
    n_HSwindow <- 250
    HistSim_QES_help <- HistSim_QES(ret[(t+n_IS-n_HSwindow):(t-1+n_IS)],alpha)
    q_FC$HistSim[t]  <- HistSim_QES_help$Q
    e_FC$HistSim[t]  <- HistSim_QES_help$ES
  }

  # GAS one factor model
  GAS1F.FC   <- Optimize_models( r= ret, alpha = alpha, model = "GAS1F", splitdate = n_IS)
  q_FC$GAS1F <- GAS1F.FC$OOSforecasts[,1]
  e_FC$GAS1F <- GAS1F.FC$OOSforecasts[,2]

  if (sum(asset_choice == assets.stocks) > 0 || sum(asset_choice == assets.indices) > 0  || sum(asset_choice == assets.foreign) > 0){
  # GAS two factor model
  GAS2F.FC   <- Optimize_models( r= ret, alpha = alpha, model = "GAS2F", splitdate = n_IS)
  q_FC$GAS2F <- GAS2F.FC$OOSforecasts[,1]
  e_FC$GAS2F <- GAS2F.FC$OOSforecasts[,2]
  }

  # ES-AS-CaviaR model
  CAREAS.FC   <- Optimize_models( r= ret, alpha = alpha, model = "CAREAS", splitdate = n_IS)
  q_FC$CAREAS <- CAREAS.FC$OOSforecasts[,1]
  e_FC$CAREAS <- CAREAS.FC$OOSforecasts[,2]

  # ES-SAV-CaViaR model
  CARESAV.FC   <- Optimize_models( r= ret, alpha = alpha, model = "CARESAV", splitdate = n_IS)
  q_FC$CARESAV <- CARESAV.FC$OOSforecasts[,1]
  e_FC$CARESAV <- CARESAV.FC$OOSforecasts[,2]

  # GAS-t model
  GAS_t.spec <- UniGASSpec(Dist = "std", GASPar = list(scale = TRUE, shape = TRUE))
  GAS_t.fit  <- UniGASFit(GAS_t.spec, data = ret[1:n_IS])
  GAS_t.FC   <- UniGASFor(GAS_t.fit, H = 1, Roll = TRUE, out = ret[-(1:n_IS)])

  q_FC$GAS_t <- as.numeric(GAS::quantile(GAS_t.FC, probs = alpha))
  e_FC$GAS_t <- as.numeric(GAS::ES(GAS_t.FC, probs = alpha))

  # unused models:
  # GARCH(1,1)-N model with fixed estimation
  # GARCH_N.spec <- ugarchspec(mean.model = list(include.mean = FALSE, armaOrder=c(0,0)) )
  # GARCH_N.fit  <- ugarchfit(spec=GARCH_N.spec, data=ret, out.sample=n_OOS)
  # GARCH_N.FC   <- ugarchforecast(GARCH_N.fit, data = NULL, n.ahead = 1, n.roll = n_OOS, out.sample = n_OOS)
  # q_FC$GARCH_N <- qnorm(alpha) * head(as.numeric(rugarch::sigma(GARCH_N.FC)[1,]), -1)
  # e_FC$GARCH_N <- -dnorm(qnorm(alpha))/alpha * head(as.numeric(rugarch::sigma(GARCH_N.FC)[1,]), -1)
  #
  # GJRGARCH(1,1)-N model with fixed estimation
  # GJRGARCH_N.spec <- ugarchspec(mean.model = list(include.mean = FALSE,armaOrder=c(0,0)), variance.model = list(model = "gjrGARCH"))
  # GJRGARCH_N.fit  <- ugarchfit(spec=GJRGARCH_N.spec, data=ret, out.sample=n_OOS)
  # GJRGARCH_N.FC   <- ugarchforecast(GJRGARCH_N.fit, data = NULL, n.ahead = 1, n.roll = n_OOS, out.sample = n_OOS)
  # q_FC$GJRGARCH_N <- qnorm(alpha) * head(as.numeric(sigma(GJRGARCH_N.FC)[1,]),-1)
  # e_FC$GJRGARCH_N <--dnorm(qnorm(alpha))/alpha *  head(as.numeric(sigma(GJRGARCH_N.FC)[1,]),-1)
  #
  # PGARCH(1,1)-N model with fixed estimation
  # PGARCH_N.spec <- ugarchspec(mean.model = list(include.mean = FALSE,armaOrder=c(0,0)), variance.model = list(model = "fGARCH",submodel = "APARCH"))
  # PGARCH_N.fit  <- ugarchfit(spec=PGARCH_N.spec, data=ret, out.sample=n_OOS)
  # PGARCH_N.FC   <- ugarchforecast(PGARCH_N.fit, data = NULL, n.ahead = 1, n.roll = n_OOS, out.sample = n_OOS)
  # q_FC$PGARCH_N <- qnorm(alpha) * head(as.numeric(sigma(PGARCH_N.FC)[1,]),-1)
  # e_FC$PGARCH_N <--dnorm(qnorm(alpha))/alpha *  head(as.numeric(sigma(PGARCH_N.FC)[1,]),-1)
  #
  # GARCH(1,1)-t model with fixed estimation
  # GARCH_t.spec <- ugarchspec(mean.model = list(include.mean = FALSE, armaOrder=c(0,0)), distribution.model = "std")
  # GARCH_t.fit  <- ugarchfit(spec=GARCH_t.spec, data=ret, out.sample=n_OOS)
  # GARCH_t.FC   <- ugarchforecast(GARCH_t.fit, data = NULL, n.ahead = 1, n.roll = n_OOS, out.sample = n_OOS)
  # q_FC$GARCH_t <- qnorm(alpha) * head(as.numeric(rugarch::sigma(GARCH_t.FC)[1,]), -1)
  # e_FC$GARCH_t <- -dnorm(qnorm(alpha))/alpha * head(as.numeric(rugarch::sigma(GARCH_t.FC)[1,]), -1)

  # PGARCH(1,1)-t model with fixed estimation
  # PGARCH_t.spec <- ugarchspec(mean.model = list(include.mean = FALSE,armaOrder=c(0,0)), variance.model = list(model = "fGARCH",submodel = "APARCH"), distribution.model = "std")
  # PGARCH_t.fit  <- ugarchfit(spec=PGARCH_t.spec, data=ret, out.sample=n_OOS)
  # PGARCH_t.FC   <- ugarchforecast(PGARCH_t.fit, data = NULL, n.ahead = 1, n.roll = n_OOS, out.sample = n_OOS)
  # q_FC$PGARCH_t <- qnorm(alpha) * head(as.numeric(sigma(PGARCH_t.FC)[1,]),-1)
  # e_FC$PGARCH_t <--dnorm(qnorm(alpha))/alpha *  head(as.numeric(sigma(PGARCH_t.FC)[1,]),-1)

  # GAS-skew t model
  # GAS_st.spec <- UniGASSpec(Dist = "sstd", GASPar = list(scale = TRUE, shape = TRUE, skew = TRUE))
  # GAS_st.fit  <- UniGASFit(GAS_st.spec, data = ret)
  # GAS_st.FC   <- UniGASFor(GAS_st.fit, H = 1, Roll = TRUE, out = ret[-(1:n_IS)])
  #
  # q_FC$GAS_st <- as.numeric(GAS::quantile(GAS_st.FC, probs = alpha))
  # e_FC$GAS_st <- as.numeric(GAS::ES(GAS_st.FC, probs = alpha))
  #
  # GAS-skew ASL model
  # GAS_ald.spec <- UniGASSpec(Dist = "ald", GASPar = list(scale = TRUE, skew = TRUE))
  # GAS_ald.fit  <- UniGASFit(GAS_ald.spec, data = ret)
  # GAS_ald.FC   <- UniGASFor(GAS_ald.fit, H = 1, Roll = TRUE, out = ret[-(1:n_IS)])
  #
  # q_FC$GAS_ald <- as.numeric(GAS::quantile(GAS_ald.FC, probs = alpha))
  # e_FC$GAS_ald <- as.numeric(GAS::ES(GAS_ald.FC, probs = alpha))

  # read out the asset return and the quantile and ES forecasts
  FC[[asset_choice]][["ret"]] <- ret
  FC[[asset_choice]][["Q"]]   <- q_FC
  FC[[asset_choice]][["ES"]]  <- e_FC
}

  # Check correlation matrices of q_FC and e_FC --------------------------------------------------------------------------------

for (i.asset in 1:n.assets){
  asset_choice <- assets[i.asset]
  ret    <- FC[[asset_choice]][["ret"]]

  q_FC <- FC[[asset_choice]][["Q"]]
  e_FC <- FC[[asset_choice]][["ES"]]

  print(asset_choice)
  print(cor(q_FC))
  print(cor(e_FC))

  cor_q <- cor(q_FC)
  cor_q[lower.tri(cor_q)] <- NA

  cor_e <- cor(e_FC)
  cor_e[lower.tri(cor_e)] <- NA

  # write correlation table
  cor_qq <- as.table(format(cor_q,digits=2))
  cor_ee <- as.table(format(cor_q,digits=2))

  corr <- cbind(dgp_names,emtpy_col,cor_qq,emtpy_col,cor_ee)

  write.table(corr, file =paste0( "tables/corr_dgp",asset_choice,"_n_",n_IS,".txt"), row.names=FALSE,
              col.names=FALSE, sep = " & ", na = " ", quote=FALSE, eol="\\\\\n")
}

  # Run the Encompassing Tests--------------------------------------------------------------------------------

  # initilize p-value results
  Encmp.res <- list(list())

  for (i.asset in 1:n.assets){

  asset_choice <- assets[i.asset]

  ret    <- FC[[asset_choice]][["ret"]]
  n_days <- length(ret)
  n_OOS  <- n_days - n_IS


  q_FC <- FC[[asset_choice]][["Q"]]
  e_FC <- FC[[asset_choice]][["ES"]]

  models_set <- 1:ncol(q_FC)
  n_models   <- length(models_set)


  VaRES.Encmp.pval <- matrix(NA,n_models,n_models)
  StrES.Encmp.pval <- matrix(NA,n_models,n_models)
  AuxES.Encmp.pval <- matrix(NA,n_models,n_models)
  VaR.Encmp.pval   <- matrix(NA,n_models,n_models)

  VaRES.Encmp.res <- array(list(),dim = c(n_models,n_models))
  StrES.Encmp.res <- array(list(),dim = c(n_models,n_models))
  AuxES.Encmp.res <- array(list(),dim = c(n_models,n_models))
  VaR.Encmp.res   <- array(list(),dim = c(n_models,n_models))

  VaRES.Encmp.loss <- matrix(NA,n_models,n_models)
  StrES.Encmp.loss <- matrix(NA,n_models,n_models)
  VaR.Encmp.loss   <- matrix(NA,n_models,n_models)
  


  for (i in 1:length(models_set)) {
    for (j in 1:length(models_set)) {
      if (i < j) {
        # Run the three ES Encompassing tests

        print(i)
        print(j)
        
        y  <- ret[(n_IS+1):n_days]
        xq <- cbind(q_FC[,i],q_FC[,j])
        xe <- cbind(e_FC[,i],e_FC[,j])
        
        test.res <- ESEncmp.Test( y = y, xq = xq, xe = xe, alpha = alpha, link = "linear", str = TRUE, 
                                  CF = 1, sparsity = "nid", bandwidth_type = "Hall-Sheather" )
        

        VaR.test.res <- VaREncmp.Test( y = y, xq = xq, alpha = alpha )

        VaRES.Encmp.pval[i,j] <- as.numeric(test.res$joint$test.out[1,2])
        VaRES.Encmp.pval[j,i] <- as.numeric(test.res$joint$test.out[2,2])

        StrES.Encmp.pval[i,j] <- as.numeric(test.res$str$test.out[1,2])
        StrES.Encmp.pval[j,i] <- as.numeric(test.res$str$test.out[2,2])

        AuxES.Encmp.pval[i,j] <- as.numeric(test.res$aux$test.out[1,2])
        AuxES.Encmp.pval[j,i] <- as.numeric(test.res$aux$test.out[2,2])

        VaR.Encmp.pval[i,j]   <- as.numeric(VaR.test.res$var$test.out[1,2])
        VaR.Encmp.pval[j,i]   <- as.numeric(VaR.test.res$var$test.out[2,2])

        
        fit.VaRES <- esregression( y ~ xq | xe, alpha = alpha, link = "linear")
        
        xq_c <- fit.VaRES$xq
        bq   <- fit.VaRES$coefficients_q
        
        q_c  <- as.numeric(xq_c %*% bq)
        
        xe_c <- fit.VaRES$xe
        be   <- fit.VaRES$coefficients_e
        
        e_c  <- as.numeric(xe_c %*% be)
        
        VaRES.Encmp.loss[i,j] <- esregression::esr_loss(r=ret[(n_IS+1):n_days], q=q_c, e=e_c, alpha=alpha, g1 = 2, g2 = 1, return_mean = TRUE)
        
        
        #diag(VaRES.Encmp.loss) <- ' '
        #VaRES.Encmp.loss[lower.tri(VaRES.Encmp.loss)] <- ' '
        
        fit.StrES <- esregression( y ~ xe | xe, alpha = alpha, link = "linear")

        xq_c <- fit.StrES$xq
        bq   <- fit.StrES$coefficients_q
        
        q_c  <- as.numeric(xq_c %*% bq)
        
        xe_c <- fit.StrES$xe
        be   <- fit.StrES$coefficients_e
        
        e_c  <- as.numeric(xe_c %*% be)      

        StrES.Encmp.loss[i,j] <- esregression::esr_loss(r=ret[(n_IS+1):n_days], q=q_c, e=e_c, alpha=alpha, g1 = 2, g2 = 1, return_mean = TRUE)
        
        #diag(StrES.Encmp.loss) <- ' '
        #StrES.Encmp.loss[lower.tri(StrES.Encmp.loss)] <- ' '
        
        
        VaR.Encmp.loss[i,j]   <- VaR.test.res$var$loss
        
        #diag(VaR.Encmp.loss) <- ' '
        #VaR.Encmp.loss[lower.tri(VaR.Encmp.loss)] <- ' '


      }
    }
  }

  Encmp.res[[asset_choice]][["VaR.Encmp.pval"]]   <- VaR.Encmp.pval
  Encmp.res[[asset_choice]][["VaRES.Encmp.pval"]] <- VaRES.Encmp.pval
  Encmp.res[[asset_choice]][["AuxES.Encmp.pval"]] <- AuxES.Encmp.pval
  Encmp.res[[asset_choice]][["StrES.Encmp.pval"]] <- StrES.Encmp.pval

  # Check whether the Encompassing tests suggests FC combination
  sig.level <- 0.1


  VaRES.Encmp.FCcomb <- matrix(0,n_models,n_models)
  AuxES.Encmp.FCcomb <- matrix(0,n_models,n_models)
  StrES.Encmp.FCcomb <- matrix(0,n_models,n_models)
  VaR.Encmp.FCcomb <- matrix(0,n_models,n_models)

  VaRES.Encmp.FCres <- matrix(NA,n_models,n_models)
  AuxES.Encmp.FCres <- matrix(NA,n_models,n_models)
  StrES.Encmp.FCres <- matrix(NA,n_models,n_models)
  VaR.Encmp.FCres <- matrix(NA,n_models,n_models)

  for (i in 1:n_models) {
    for (j in 1:n_models) {

      VaRES.Encmp.FCcomb[i,j] <- as.numeric((VaRES.Encmp.pval[i,j] < sig.level) & (VaRES.Encmp.pval[j,i] < sig.level))
      AuxES.Encmp.FCcomb[i,j] <- as.numeric((AuxES.Encmp.pval[i,j] < sig.level) & (AuxES.Encmp.pval[j,i] < sig.level))
      StrES.Encmp.FCcomb[i,j] <- as.numeric((StrES.Encmp.pval[i,j] < sig.level) & (StrES.Encmp.pval[j,i] < sig.level))
      VaR.Encmp.FCcomb[i,j]   <- as.numeric((VaR.Encmp.pval[i,j] < sig.level) & (VaR.Encmp.pval[j,i] < sig.level))

      if (i < j) {
        if ((VaRES.Encmp.pval[i,j] < sig.level) & (VaRES.Encmp.pval[j,i] < sig.level)){
          VaRES.Encmp.FCres[i,j] <- 3
        } else if ((VaRES.Encmp.pval[i,j] < sig.level) & (VaRES.Encmp.pval[j,i] >= sig.level)){
          VaRES.Encmp.FCres[i,j] <- 1
        } else if ((VaRES.Encmp.pval[i,j] >= sig.level) & (VaRES.Encmp.pval[j,i] < sig.level)){
          VaRES.Encmp.FCres[i,j] <- 2
        } else {
          VaRES.Encmp.FCres[i,j] <- 0
        }

        if ((AuxES.Encmp.pval[i,j] < sig.level) & (AuxES.Encmp.pval[j,i] < sig.level)){
          AuxES.Encmp.FCres[i,j] <- 3
        } else if ((AuxES.Encmp.pval[i,j] < sig.level) & (AuxES.Encmp.pval[j,i] >= sig.level)){
          AuxES.Encmp.FCres[i,j] <- 1
        } else if ((AuxES.Encmp.pval[i,j] >= sig.level) & (AuxES.Encmp.pval[j,i] < sig.level)){
          AuxES.Encmp.FCres[i,j] <- 2
        } else {
          AuxES.Encmp.FCres[i,j] <- 0
        }

        if ((StrES.Encmp.pval[i,j] < sig.level) & (StrES.Encmp.pval[j,i] < sig.level)){
          StrES.Encmp.FCres[i,j] <- 3
        } else if ((StrES.Encmp.pval[i,j] < sig.level) & (StrES.Encmp.pval[j,i] >= sig.level)){
          StrES.Encmp.FCres[i,j] <- 1
        } else if ((StrES.Encmp.pval[i,j] >= sig.level) & (StrES.Encmp.pval[j,i] < sig.level)){
          StrES.Encmp.FCres[i,j] <- 2
        } else {
          StrES.Encmp.FCres[i,j] <- 0
        }

        if ((VaR.Encmp.pval[i,j] < sig.level) & (VaR.Encmp.pval[j,i] < sig.level)){
          VaR.Encmp.FCres[i,j] <- 3
        } else if ((VaR.Encmp.pval[i,j] < sig.level) & (VaR.Encmp.pval[j,i] >= sig.level)){
          VaR.Encmp.FCres[i,j] <- 1
        } else if ((VaRES.Encmp.pval[i,j] >= sig.level) & (VaR.Encmp.pval[j,i] < sig.level)){
          VaR.Encmp.FCres[i,j] <- 2
        } else {
          VaR.Encmp.FCres[i,j] <- 0
        }
      }
    }
  }

  Encmp.res[[asset_choice]][["VaRES.Encmp.FCcomb"]] <- VaRES.Encmp.FCcomb
  Encmp.res[[asset_choice]][["VaR.Encmp.FCcomb"]]   <- VaR.Encmp.FCcomb
  Encmp.res[[asset_choice]][["AuxES.Encmp.FCcomb"]] <- AuxES.Encmp.FCcomb
  Encmp.res[[asset_choice]][["StrES.Encmp.FCcomb"]] <- StrES.Encmp.FCcomb

  Encmp.res[[asset_choice]][["VaRES.Encmp.FCres"]] <- VaRES.Encmp.FCres
  Encmp.res[[asset_choice]][["VaR.Encmp.FCres"]]   <- VaR.Encmp.FCres
  Encmp.res[[asset_choice]][["AuxES.Encmp.FCres"]] <- AuxES.Encmp.FCres
  Encmp.res[[asset_choice]][["StrES.Encmp.FCres"]] <- StrES.Encmp.FCres




  # write tables: p-values

  # define stars for double rejection

  mystars_VaRES <- ifelse(VaRES.Encmp.FCcomb == 1, "*" , ifelse(is.na(VaRES.Encmp.FCcomb)," ", " "))
  mystars_VaR <- ifelse(VaR.Encmp.FCcomb == 1, "*" , " ")
  mystars_AuxES <- ifelse(AuxES.Encmp.FCcomb == 1, "*" , " ")
  mystars_StrES <- ifelse(StrES.Encmp.FCcomb == 1, "*" , " ")


  VaRES.pval <- format(round(as.table(VaRES.Encmp.pval),digits = 3),nsmall = 3)
  VaRES      <- matrix(paste(VaRES.pval, mystars_VaRES, sep=""), ncol=ncol(VaRES.pval))
  diag(VaRES) <- NA

  VaR.pval <- format(round(as.table(VaR.Encmp.pval),digits = 3),nsmall = 3)
  VaR      <- matrix(paste(VaR.pval, mystars_VaR, sep=""), ncol=ncol(VaR.pval))
  diag(VaR) <- NA

  p_val1 <- cbind(dgp_names,emtpy_col,VaRES,emtpy_col,VaR)


  write.table(p_val1, file =paste0( "tables/pval_Joint_VaR_dgp_",asset_choice,"_n_",n_IS,".txt"), row.names=FALSE,
              col.names=FALSE, sep = " & ", na = " ", quote=FALSE, eol="\\\\\n")


  AuxES.pval <- format(round(as.table(AuxES.Encmp.pval),digits = 3),nsmall = 3)
  AuxES      <- matrix(paste(AuxES.pval, mystars_AuxES, sep=""), ncol=ncol(AuxES.pval))
  diag(AuxES) <- NA

  StrES.pval <- format(round(as.table(StrES.Encmp.pval),digits = 3),nsmall = 3)
  StrES      <- matrix(paste(StrES.pval, mystars_StrES, sep=""), ncol=ncol(StrES.pval))
  diag(StrES) <- NA


  p_val2 <- cbind(dgp_names,emtpy_col,AuxES,emtpy_col,StrES)

  write.table(p_val2, file =paste0( "tables/pval_Aux_Str_dgp_",asset_choice,"_n_",n_IS,".txt"), row.names=FALSE,
              col.names=FALSE, sep = " & ", na = " ", quote=FALSE, eol="\\\\\n")
  


  # Table: Encompassing Test results

  VaRES.Test <- matrix(NA,n_models,4)
  AuxES.Test <- matrix(NA,n_models,4)
  StrES.Test <- matrix(NA,n_models,4)
  VaR.Test   <- matrix(NA,n_models,4)


  for (j in 1:n_models) {


    # 1. VaRES

    row_j <- VaRES.Encmp.FCres[j,]
    col_j <- VaRES.Encmp.FCres[,j]

    # NR
    NR_r <- row_j == 0
    NR_c <- col_j == 0

    models_used <- sum(!is.na(row_j)) + sum(!is.na(col_j))

    VaRES.Test[j,1] = 100*round((sum(NR_r,na.rm = TRUE) + sum(NR_c,na.rm = TRUE))/models_used, 2)

    # E1
    E1_r <- row_j == 1
    E1_c <- col_j == 2

    VaRES.Test[j,2] = 100*round((sum(E1_r,na.rm = TRUE) + sum(E1_c,na.rm = TRUE))/models_used ,2)

    # E2
    E2_r <- row_j == 2
    E2_c <- col_j == 1

    VaRES.Test[j,3] = 100*round((sum(E2_r,na.rm = TRUE) + sum(E2_c,na.rm = TRUE))/models_used ,2)

    # C
    C_r <- row_j == 3
    C_c <- col_j == 3

    VaRES.Test[j,4] = 100*round((sum(C_r,na.rm = TRUE) + sum(C_c,na.rm = TRUE))/models_used ,2)


    # 2. VaR

    row_j <- VaR.Encmp.FCres[j,]
    col_j <- VaR.Encmp.FCres[,j]

    models_used <-  sum(!is.na(row_j)) + sum(!is.na(col_j))

    # NR
    NR_r <- row_j == 0
    NR_c <- col_j == 0

    VaR.Test[j,1] = 100*round((sum(NR_r,na.rm = TRUE) + sum(NR_c,na.rm = TRUE))/models_used, 2)

    # E1
    E1_r <- row_j == 1
    E1_c <- col_j == 2

    VaR.Test[j,2] = 100*round((sum(E1_r,na.rm = TRUE) + sum(E1_c,na.rm = TRUE))/models_used ,2)

    # E2
    E2_r <- row_j == 2
    E2_c <- col_j == 1

    VaR.Test[j,3] = 100*round((sum(E2_r,na.rm = TRUE) + sum(E2_c,na.rm = TRUE))/models_used ,2)

    # C
    C_r <- row_j == 3
    C_c <- col_j == 3

    VaR.Test[j,4] = 100*round((sum(C_r,na.rm = TRUE) + sum(C_c,na.rm = TRUE))/models_used ,2)


    # 3. AuxES

    row_j <- AuxES.Encmp.FCres[j,]
    col_j <- AuxES.Encmp.FCres[,j]

    models_used <-  sum(!is.na(row_j)) + sum(!is.na(col_j))

    # NR
    NR_r <- row_j == 0
    NR_c <- col_j == 0

    AuxES.Test[j,1] = 100*round((sum(NR_r,na.rm = TRUE) + sum(NR_c,na.rm = TRUE))/models_used, 2)

    # E1
    E1_r <- row_j == 1
    E1_c <- col_j == 2

    AuxES.Test[j,2] = 100*round((sum(E1_r,na.rm = TRUE) + sum(E1_c,na.rm = TRUE))/models_used ,2)

    # E2
    E2_r <- row_j == 2
    E2_c <- col_j == 1

    AuxES.Test[j,3] = 100*round((sum(E2_r,na.rm = TRUE) + sum(E2_c,na.rm = TRUE))/models_used ,2)

    # C
    C_r <- row_j == 3
    C_c <- col_j == 3

    AuxES.Test[j,4] = 100*round((sum(C_r,na.rm = TRUE) + sum(C_c,na.rm = TRUE))/models_used ,2)


    # 4. StrES

    row_j <- StrES.Encmp.FCres[j,]
    col_j <- StrES.Encmp.FCres[,j]

    models_used <-  sum(!is.na(row_j)) + sum(!is.na(col_j))

    # NR
    NR_r <- row_j == 0
    NR_c <- col_j == 0

    StrES.Test[j,1] = 100*round((sum(NR_r,na.rm = TRUE) + sum(NR_c,na.rm = TRUE))/models_used, 2)

    # E1
    E1_r <- row_j == 1
    E1_c <- col_j == 2

    StrES.Test[j,2] = 100*round((sum(E1_r,na.rm = TRUE) + sum(E1_c,na.rm = TRUE))/models_used ,2)

    # E2
    E2_r <- row_j == 2
    E2_c <- col_j == 1

    StrES.Test[j,3] = 100*round((sum(E2_r,na.rm = TRUE) + sum(E2_c,na.rm = TRUE))/models_used ,2)

    # C
    C_r <- row_j == 3
    C_c <- col_j == 3

    StrES.Test[j,4] = 100*round((sum(C_r,na.rm = TRUE) + sum(C_c,na.rm = TRUE))/models_used ,2)

  }

  # omit zeros:
  VaRES.Test <- ifelse(VaRES.Test==0," ",VaRES.Test)
  VaR.Test   <- ifelse(VaR.Test==0," ",VaR.Test)
  StrES.Test <- ifelse(StrES.Test==0," ",StrES.Test)
  AuxES.Test <- ifelse(AuxES.Test==0," ",AuxES.Test)


  Emp_Res1 <- cbind(emtpy_col,emtpy_col,dgp_names,emtpy_col,VaRES.Test,emtpy_col,VaR.Test)

  write.table(Emp_Res1, file =paste0( "tables/Emp_Joint_VaR_dgp_",asset_choice,"_n_",n_IS,".txt"), row.names=FALSE,
              col.names=FALSE, sep = " & ", na = " ", quote=FALSE, eol="\\\\\n")


  Emp_Res2 <- cbind(emtpy_col,emtpy_col,dgp_names,emtpy_col,AuxES.Test,emtpy_col,StrES.Test)

  write.table(Emp_Res2, file =paste0( "tables/Emp_Aux_Str_dgp_",asset_choice,"_n_",n_IS,".txt"), row.names=FALSE,
              col.names=FALSE, sep = " & ", na = " ", quote=FALSE, eol="\\\\\n")
  
 
  
  Emp_Res <- cbind(dgp_names,emtpy_col,StrES.Test,emtpy_col,AuxES.Test,emtpy_col,VaRES.Test,emtpy_col,VaR.Test)
  
  write.table(Emp_Res, file =paste0( "tables/Emp_dgp_",asset_choice,"_n_",n_IS,".txt"), row.names=FALSE,
              col.names=FALSE, sep = " & ", na = " ", quote=FALSE, eol="\\\\\n")
  
  
  # report scoring functions
  
  # (1) run loss of individual forecast
  
  VaRES.loss.in <- matrix(NA,n_models,1)
  StrES.loss.in <- matrix(NA,n_models,1)
  VaR.loss.in   <- matrix(NA,n_models,1)
  
  for (i in 1:n_models){
    
    VaRES.loss.in[i] <- esregression::esr_loss(r=ret[(n_IS+1):n_days], q=q_FC[,i], e=e_FC[,i], alpha=alpha, g1 = 2, g2 = 1, return_mean = TRUE)
    StrES.loss.in[i] <- esregression::esr_loss(r=ret[(n_IS+1):n_days], q=e_FC[,i], e=e_FC[,i], alpha=alpha, g1 = 2, g2 = 1, return_mean = TRUE)
    VaR.loss.in[i]   <- esencmp::Quantile_loss(r=ret[(n_IS+1):n_days], v=q_FC[,i], alpha=alpha, return_mean = TRUE)
    
  }
  
  if (asset_choice == "IBM"){
    VaR.loss.in    <- 10*as.vector(VaR.loss.in)
    VaR.Encmp.loss <- 10*VaR.Encmp.loss
  } else if (asset_choice == "DAX"){
    VaR.loss.in    <- 100*as.vector(VaR.loss.in)
    VaR.Encmp.loss <- 100*VaR.Encmp.loss
  } else if (asset_choice == "SP500"){
    VaR.loss.in    <- 100*as.vector(VaR.loss.in)
    VaR.Encmp.loss <- 100*VaR.Encmp.loss
  } else {
    VaR.loss.in   <- as.vector(VaR.loss.in)
  }
  
  
  VaRES.loss.in <- as.vector(VaRES.loss.in)
  StrES.loss.in <- as.vector(StrES.loss.in)
  
  # (2) create table
  header_VaRES <- cbind(NA, NA, NA, NA, format(round(as.table(t(VaRES.loss.in)),digits = 3),nsmall = 3))
  header_VaR   <- cbind(NA, NA, NA, NA, format(round(as.table(t(VaR.loss.in)),digits = 3),nsmall = 3))
  header_StrES <- cbind(NA, NA, NA, NA, format(round(as.table(t(StrES.loss.in)),digits = 3),nsmall = 3))
  
  header <- cbind(NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA,NA)
  
  #VaRES.loss <- rbind(header_VaRES,header,cbind(dgp_names,emtpy_col,format(round(as.table(VaRES.loss.in),digits = 3),nsmall = 3),
  #                                       emtpy_col,format(round(as.table(VaRES.Encmp.loss),digits = 3),nsmall = 3)))
  #VaR.loss   <- rbind(header_VaR,header,  cbind(dgp_names,emtpy_col,format(round(as.table(VaR.loss.in),digits = 3),nsmall = 3),
  #                                       emtpy_col,format(round(as.table(VaR.Encmp.loss),digits = 3),nsmall = 3)))
  #StrES.loss <- rbind(header_StrES,header,cbind(dgp_names,emtpy_col,format(round(as.table(StrES.loss.in),digits = 3),nsmall = 3),
  #                                       emtpy_col,format(round(as.table(StrES.Encmp.loss),digits = 3),nsmall = 3)))
  
  
  VaRES.loss <- cbind(dgp_names,emtpy_col,format(round(as.table(VaRES.loss.in),digits = 3),nsmall = 3),
                      emtpy_col,format(round(as.table(VaRES.Encmp.loss),digits = 3),nsmall = 3))
  VaR.loss   <- cbind(dgp_names,emtpy_col,format(round(as.table(VaR.loss.in),digits = 3),nsmall = 3),
                      emtpy_col,format(round(as.table(VaR.Encmp.loss),digits = 3),nsmall = 3))
  StrES.loss <- cbind(dgp_names,emtpy_col,format(round(as.table(VaRES.loss.in),digits = 3),nsmall = 3),
                      emtpy_col,format(round(as.table(StrES.Encmp.loss),digits = 3),nsmall = 3))
  
  VaRES.loss <- as.table(VaRES.loss)
  VaR.loss   <- as.table(VaR.loss)
  StrES.loss <- as.table(StrES.loss)
  
  
  write.table(VaRES.loss, file =paste0( "tables/Loss_Joint_Aux_dgp_",asset_choice,"_n_",n_IS,".txt"), row.names=FALSE,
              col.names=FALSE, sep = " & ", na = " ", quote=FALSE, eol="\\\\\n")
  
  write.table(VaR.loss, file =paste0( "tables/Loss_VaR_dgp_",asset_choice,"_n_",n_IS,".txt"), row.names=FALSE,
              col.names=FALSE, sep = " & ", na = " ", quote=FALSE, eol="\\\\\n") 
  
  write.table(StrES.loss, file =paste0( "tables/Loss_Str_dgp_",asset_choice,"_n_",n_IS,".txt"), row.names=FALSE,
              col.names=FALSE, sep = " & ", na = " ", quote=FALSE, eol="\\\\\n")

  



}