# .serm(list = ls())
library(foreach)
library(doParallel)
library(quantreg)
library(esregression)
library(esencmp)
library(abind)
library(magrittr)
library(dplyr)

source("functions_DGP.R")


## Set simulation parameters here ---------------------------------------------------------------------------------------------------------------------
M.MC   <- 1000
alpha  <- 0.025
T.set  <- c(500, 1000, 2500, 5000)
m_rho  <- 21
DGPs   <- c("N-GARCH-N-GJR","GAS","CARE","GAS_ES")  
links  <- c("linear")
g2_fct <- c(1,2,3)     # g2=1 is the default

# Weighting parameter of the two forecasts
rho_values      <- seq(0,1,length.out=m_rho)
test_sig_levels <- c(0.01, 0.05, 0.1)
test_functional <- c("VaRES", "AuxES","StrES")
test_dir        <- c(1,2)

str <- TRUE

## Paralell Simulation ---------------------------------------------------------------------------------------------------------------------
cl <- makeCluster(min(parallel:::detectCores()-1, M.MC) )
registerDoParallel(cl)
start_time <- Sys.time()
Test.df.MC <- foreach (i_MC = 1:M.MC, .combine=rbind) %dopar% {
  library(esregression)
  library(esencmp)
  library(quantreg)
  library(quadprog)
  library(MASS)

  source("functions_DGP.R")

  Test.df <- data.frame(matrix(ncol=10,nrow=0))
  colnames(Test.df) <- c("i_MC", "DGP", "T", "link", "combine.method", "G2", "functional", "direction", "rho", "pval")

   for (DGP in DGPs){
    for (T in T.set){
        
      if (DGP == "N-GARCH-N-GJR"){
        combine_type = "variance_combination"
      } else {
        combine_type = "Bernoulli"
      }   

      # simulate the data here
      data_sim <- sim_DGP(T=T, DGP=DGP, rho_values=rho_values, alpha=alpha, combine_type = combine_type)
      return_matrix <- data_sim$return_matrix
      VaR_FC_1 <- data_sim$VaR_FC_1
      ES_FC_1  <- data_sim$ES_FC_1
      VaR_FC_2 <- data_sim$VaR_FC_2
      ES_FC_2  <- data_sim$ES_FC_2
  
      xq <- cbind(VaR_FC_1,VaR_FC_2)
      xe <- cbind(ES_FC_1,ES_FC_2)
  
      for (link in links){
        for (g2 in g2_fct){
          for (i_rho in 1:m_rho){
  
            res_encmp <- tryCatch(ESEncmp.Test(y = return_matrix[i_rho,], xq = xq, xe = xe,
                                               alpha = alpha, link = link, g2 = g2, str = str,
                                               CF = 1, sparsity = "nid", bandwidth_type = "Hall-Sheather"),
                                  error=function(e) NULL)
  
            if (!is.null(res_encmp)) {
  
              for (i_testdir in 1:2){
  
                Test.df <- rbind(Test.df, data.frame(i_MC =i_MC, DGP = DGP, T = T, link = link, combine.method = combine_type,
                                                     G2 = g2, functional = "VaRES", direction = i_testdir,
                                                     rho = rho_values[i_rho], pval = as.numeric(res_encmp$joint$test.out[i_testdir,2])))
  
                Test.df <- rbind(Test.df, data.frame(i_MC =i_MC, DGP = DGP, T = T, link = link, combine.method = combine_type,
                                                     G2 = g2, functional = "AuxES", direction = i_testdir,
                                                     rho = rho_values[i_rho], pval = as.numeric(res_encmp$aux$test.out[i_testdir,2])))
  
                Test.df <- rbind(Test.df, data.frame(i_MC =i_MC, DGP = DGP, T = T, link = link, combine.method = combine_type,
                                                     G2 = g2, functional = "StrES", direction = i_testdir,
                                                     rho = rho_values[i_rho], pval = as.numeric(res_encmp$str$test.out[i_testdir,2])))
              
              }
  
            } else {
              warning( paste0("ESEncmp.Test failed for DGP=", DGP, ", T=", T, ", link=", link, ", i_rho=", i_rho))
            }
          }
        }
      }
    }
  }

  Test.df

}
stopCluster(cl)
run_time <- Sys.time() - start_time

head(Test.df.MC,10)

dir.create("data_sim", showWarnings = FALSE)
saveRDS(Test.df.MC, file = "data_sim/power_sim_loss.rds")




