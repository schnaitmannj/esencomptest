# ESEncompTest

This repository contains the replication codes for the papers "Forecast Encompassing Tests for the Expected Shortfall" (Dimitriadis and Schnaitmann, 2021) and "Encompassing Tests for Value at Risk and Expected Shortfall Multi-Step Forecasts based on Inference on the Boundary" (Dimitriadis, Liu and Schnaitmann, 2021+).


Installation
------------

This package requires the [esregression](https://gitlab.com/schnaitmannj/esregression) package which can be installed using

    install.packages("devtools")
    devtools::install_gitlab("schnaitmannj/esregression")

and the [esencmp](https://gitlab.com/schnaitmannj/esregression) package which can be installed using

    devtools::install_gitlab("schnaitmannj/esencmp")

Forecast Encompassing Tests for the Expected Shortfall
------------------------------------------------------

The replication code is given in the [FET](https://gitlab.com/schnaitmannj/esencomptest/-/tree/master/FET) folder. The simulation results can be found in the folder [simulation](https://gitlab.com/schnaitmannj/esencomptest/-/tree/master/FET/simulation). To replicate the Monte Carlo study results run

    # baseline results
    source("power_sim.R")    

    # additional link functions       
    source("power_sim_affine.R")
    source("power_sim_nonlinear.R")

    # additional loss functions
    source("power_sim_loss.R")

    # reproduces all simulation tables in the paper
    source("size_tables_paper.R")

    # reproduces the power plots in the paper
    source("power_plots_RR_IJF.R")
    
    # reproduces the figure in the appendix
    source("strength_misspecification.R")

The empirical application is given in the folder [application](https://gitlab.com/schnaitmannj/esencomptest/-/tree/master/FET/application). To replicate the results, run

    source("empirical_application.R")



Encompassing Tests for Value at Risk and Expected Shortfall Multi-Step Forecasts based on Inference on the Boundary
-------------------------------------------------------------------------------------------------------------------   

The replication code is given in the [BoundaryTest](https://gitlab.com/schnaitmannj/esencomptest/-/tree/master/BoundaryTest) folder. The simulation results can be found in the folder [simulation](https://gitlab.com/schnaitmannj/esencomptest/-/tree/master/BoundaryTest/simulation). To replicate the Monte Carlo study results run

    # one-step ahead forecasts
    source("power_sim.R")    

    # multi-step ahead and aggregate forecasts      
    source("power_sim_multistep.R")

    # reproduces all simulation tables in the paper
    source("size_tables_paper.R")
    source("size_tables_multistep.R")

    # reproduces the (adjusted) power plots in the paper
    source("power_functions_paper.R")
    source("power_functions_multistep_paper.R")
    source("power_functions_multistep_appendix.R")

    # reproduces the (adjusted) power plots in the supplementary material
    source("power_functions_SA_paper.R")
    source("power_functions_SA_multistep_paper.R")
    source("power_functions_SA_multistep_appendix.R")

    # reproduces the distribution of the combination weights plot
    source("Plot_distribution_rbind.R")
    source("Plot_distribution_paper.R")


References
----------

[Forecast Encompassing Test for the Expected Shortfall](https://doi.org/10.1016/j.ijforecast.2020.07.008)

[Encompassing Tests for Value at Risk and Expected Shortfall Multi-Step Forecasts based on Inference on the Boundary](https://arxiv.org/abs/2009.07341)

